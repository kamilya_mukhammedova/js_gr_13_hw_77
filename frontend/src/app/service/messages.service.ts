import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, MessageData } from '../../model/message.model';
import { environment } from '../../environments/environment';
import { map, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messagesUploading = new Subject<boolean>();
  private messagesArray: Message[] = [];

  constructor(private http: HttpClient) { }

  getMessages() {
    this.messagesFetching.next(true);
    this.http.get<Message[]>(environment.apiUrl + '/messages').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(messageData => {
          return new Message(
            messageData.id,
            messageData.author,
            messageData.message,
            messageData.image,
          );
        });
      }))
      .subscribe({
        next: messages => {
          this.messagesArray = messages;
          this.messagesChange.next(this.messagesArray.slice());
          this.messagesFetching.next(false);
        },
        error: error => {
          this.messagesFetching.next(false);
        }
      });
  }

  postNewMessage(messageData: MessageData) {
    const formData = new FormData();
    Object.keys(messageData).forEach(key => {
      if(messageData[key] !== null) {
        formData.append(key, messageData[key]);
      }
    });
    this.messagesUploading.next(true);
    return this.http.post(environment.apiUrl + '/messages', formData)
      .pipe(tap(() => {
        this.messagesUploading.next(false);
      }, () => {
        this.messagesUploading.next(false);
      }));
  }
}
