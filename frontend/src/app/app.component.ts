import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NewMessageComponent } from './ui/new-message/new-message.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(NewMessageComponent, {
      width: '600px',
    });
  }
}
