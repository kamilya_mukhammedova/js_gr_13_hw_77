import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../../model/message.model';
import { MessagesService } from '../service/messages.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages!: Message[];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private messagesService: MessagesService) {}

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messagesService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages.reverse();
    });
    this.messagesFetchingSubscription = this.messagesService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.messagesService.getMessages();
  }

  ngOnDestroy(): void {
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
  }
}
