import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../../service/messages.service';
import { Router } from '@angular/router';
import { MessageData } from '../../../model/message.model';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  messagesUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(
    public dialogRef: MatDialogRef<NewMessageComponent>,
    private messagesService: MessagesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.messagesUploadingSubscription = this.messagesService.messagesUploading.subscribe((isUploading:boolean) => {
      this.isUploading = isUploading;
    });
  }

  onSubmit() {
    const messageData: MessageData = this.form.value;
    this.messagesService.postNewMessage(messageData).subscribe(() => {
      this.messagesService.getMessages();
      this.dialogRef.close();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.messagesUploadingSubscription.unsubscribe();
  }
}
